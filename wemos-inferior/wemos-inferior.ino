
#include <ESP8266WiFi.h>
#include <WiFiClient.h> 
#include <ESP8266HTTPClient.h>

byte ledPin = 2;
char ssid[] = "Baston";           // SSID of your AP
char pass[] = "12345678";         // password of your AP

IPAddress server(192,168,4,15);     // IP address of the AP
WiFiClient client;

// SENSOR ULTRASONICO
#define TRIGGER 5
#define ECHO 4

//RFID
#include <SPI.h>
#include <MFRC522.h>
#define RST_PIN  0
#define SS_PIN  16

MFRC522 mfrc522(SS_PIN, RST_PIN); ///Creamos el objeto para el RC522

const float sonido = 34300.0; // Velocidad del sonido en cm/s
const float umbral1 = 100.0;
const float umbral2 = 50.0;
const float umbral3 = 1.0;

void setup() {
  Serial.begin(9600);

  pinMode(TRIGGER, OUTPUT);
  delayMicroseconds(10);

  pinMode(ECHO, INPUT);
  delayMicroseconds(10);
  
  SPI.begin();        //Iniciamos el Bus SPI
  mfrc522.PCD_Init(); // Iniciamos el MFRC522
  
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, pass);           // connects to the WiFi AP
  Serial.println();
  Serial.println("Connection to the AP");
  while (WiFi.status() != WL_CONNECTED) {
    Serial.print(".");
    delay(500);
  }
  Serial.println();
  Serial.println("Connected");
  Serial.print("LocalIP:"); Serial.println(WiFi.localIP());
  Serial.println("MAC:" + WiFi.macAddress());
  Serial.print("Gateway:"); Serial.println(WiFi.gatewayIP());
  Serial.print("AP MAC:"); Serial.println(WiFi.BSSIDstr());
  pinMode(ledPin, OUTPUT);
}

byte ActualUID[4]; //almacenará el código de la etiqueta leída
byte Ubicacion1[4]= {0x8B, 0xA1, 0x95, 0x21} ; //código de la ubicación 1
byte Ubicacion2[4]= {0x76, 0xC9, 0x5B, 0x1F} ; //código de la ubicación 2
byte Ubicacion3[4]= {0xFD, 0xE5, 0xFB, 0xA9} ; //código de la ubicación 3
byte Ubicacion4[4]= {0xD6, 0x6B, 0xFC, 0xA9} ; //código de la ubicación 4
byte Ubicacion5[4]= {0xD5, 0x29, 0xFC, 0xA9} ; //código de la ubicación 5
byte Ubicacion6[4]= {0xF1, 0x3F, 0xFC, 0xA9} ; //código de la ubicación 6
byte Ubicacion7[4]= {0x95, 0x53, 0xFC, 0xA9} ; //código de la ubicación 7

void loop() {

  iniciarTrigger();
  // Obtenemos la distancia
  float distancia = calcularDistancia();
  
  
  // Lanzamos alerta si estamos dentro del rango de peligro
  if (distancia < umbral1 && distancia > umbral2){
    client.connect(server, 80);
    digitalWrite(ledPin, LOW);
    Serial.print("Buzzer");
    client.print("buzzerT");
    digitalWrite(ledPin, HIGH);
    client.stop();
    delay(500);
  }
  if (distancia < umbral2 && distancia > 1){
    client.connect(server, 80);
    digitalWrite(ledPin, LOW);
    Serial.print("Vibrar");
    client.print("vibrarT");
    digitalWrite(ledPin, HIGH);
    client.stop();
    delay(500);
  }



  // Revisamos si hay nuevas tarjetas  presentes
  if ( mfrc522.PICC_IsNewCardPresent()) 
        {  
          Serial.print("Tarjeta+++++++:");
      //Seleccionamos una tarjeta
            if ( mfrc522.PICC_ReadCardSerial()) 
            {
                  // Enviamos serialemente su UID
                  Serial.print(F("Card UID:"));
                  for (byte i = 0; i < mfrc522.uid.size; i++) {
                          Serial.print(mfrc522.uid.uidByte[i] < 0x10 ? " 0" : " ");
                          Serial.print(mfrc522.uid.uidByte[i], HEX);   
                          ActualUID[i]=mfrc522.uid.uidByte[i];          
                  } 
                  Serial.print("     ");                 
                  //comparamos los UID para determinar si es uno de nuestros usuarios  
                  if(compareArray(ActualUID,Ubicacion1))
                    {
                    Serial.println("DIRECCIÓN DE CARRERA-");
                    enviar("1");
                    }
                    
                  else if(compareArray(ActualUID,Ubicacion2))
                   {
                   Serial.println("LABORATORIO 1");
                    enviar("2");
                   }   
                    
                  else if(compareArray(ActualUID,Ubicacion3))
                   {
                   Serial.println("LABORATORIO 2");
                    enviar("3");
                    }
                  
                  else if(compareArray(ActualUID,Ubicacion4))
                   {
                   Serial.println("LABORATORIO 3");
                    enviar("4");
                   }

                    else if(compareArray(ActualUID,Ubicacion5))
                   {
                   Serial.println("AULA 1");
                    enviar("5");
                   }
                   
                    else if(compareArray(ActualUID,Ubicacion6))
                   {
                   Serial.println("AULA 2");
                    enviar("6");
                   }

                    else if(compareArray(ActualUID,Ubicacion7))
                   {
                   Serial.println("AULA 3");
                    enviar("7");
                   }
                    
                  else
                  Serial.println("UBICACION NO REGISTRADA"); // lectura que aparecerá si colocamos un tag que no esta registrado.

                 
                  
                  // Terminamos la lectura de la tarjeta tarjeta  actual
                  mfrc522.PICC_HaltA();
          
            }
      
  }
}




// Método que calcula la distancia a la que se encuentra un objeto.
// Devuelve una variable tipo float que contiene la distancia
float calcularDistancia()
{
  
  // La función pulseIn obtiene el tiempo que tarda en cambiar entre estados, en este caso a HIGH
  long tiempo = 0;
  tiempo = pulseIn(ECHO, HIGH, 200*2*29);

  // Obtenemos la distancia en cm, hay que convertir el tiempo en segudos ya que está en microsegundos
  // por eso se multiplica por 0.000001
  float distancia = 0;
    
  if (tiempo<0){
    distancia = 5555;
    Serial.print(distancia);
    Serial.print("cm");
    Serial.println();
    delay(200);
      
  }else {
    distancia = tiempo * 0.000001 * sonido / 2.0;
    Serial.print(distancia);
    Serial.print("cm");
    Serial.println();
    delay(200);
  
  }
  
  return distancia;
}

// Método que inicia la secuencia del Trigger para comenzar a medir
void iniciarTrigger()
{
  
  // Ponemos el Triiger en estado bajo y esperamos 2 ms
  digitalWrite(TRIGGER, LOW);
  delayMicroseconds(2);

  // Ponemos el pin Trigger a estado alto y esperamos 10 ms
  digitalWrite(TRIGGER, HIGH);
  delayMicroseconds(10);

  // Comenzamos poniendo el pin Trigger en estado bajo
  digitalWrite(TRIGGER, LOW);
}



void enviar(String contador) {
  client.connect(server, 80);
  digitalWrite(ledPin, LOW);
  Serial.print("Reproducir");
  client.print(contador+"T");
  digitalWrite(ledPin, HIGH);
  client.stop();
  delay(500);
}

//Función para comparar dos vectores
 boolean compareArray(byte array1[],byte array2[])
{
  if(array1[0] != array2[0])return(false);
  if(array1[1] != array2[1])return(false);
  if(array1[2] != array2[2])return(false);
  if(array1[3] != array2[3])return(false);
  return(true);
}
