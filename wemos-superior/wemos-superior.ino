#include <ESP8266WiFi.h>

WiFiServer server(80);
IPAddress IP(192,168,4,15);
IPAddress mask = (255, 255, 255, 0);


#define BUZZER D7 
const int RELE = 16;

//MP3
#include "Arduino.h"  // 
#include "SoftwareSerial.h"
#include "DFRobotDFPlayerMini.h"
SoftwareSerial mySoftwareSerial(D5, D6); // RX, TX
DFRobotDFPlayerMini myDFPlayer;
int volumen = 20;

void setup() {
  pinMode(RELE, OUTPUT);
  digitalWrite(RELE, HIGH);

  // Iniciamos el monitor serie
  Serial.begin(9600);

  mySoftwareSerial.begin(9600);
  if (!myDFPlayer.begin(mySoftwareSerial)) {  // Se usa el software serial para comunicacion con el mp3//Use softwareSerial to communicate with mp3
    Serial.println("No se logro inicializar el modulo mp3");
    while(true);
  }
  myDFPlayer.volume(volumen);  // Setear volumen
  
  Serial.begin(9600);
  WiFi.mode(WIFI_AP);
  WiFi.softAP("Baston", "12345678");
  WiFi.softAPConfig(IP, IP, mask);
  server.begin();
  Serial.println();
  Serial.println("Server started.");
  Serial.print("IP: ");     Serial.println(WiFi.softAPIP());
  Serial.print("MAC:");     Serial.println(WiFi.softAPmacAddress());
  
}

void loop() {
  WiFiClient client = server.available();
  if (!client) {return;}
  String request = client.readStringUntil('T');
  Serial.println("********************************");
  if (request == "vibrar"){
    tone(BUZZER, 2500, 200);
    digitalWrite(RELE, LOW);
    delay(100);
    digitalWrite(RELE, HIGH);
    delay(500);
  }else if (request == "buzzer"){
    tone(BUZZER, 2500, 200);
  }else{
      Serial.println("Reproducir");
      request.remove(request.length());
      Serial.println(request);
      myDFPlayer.play(request.toInt());
  }
}
